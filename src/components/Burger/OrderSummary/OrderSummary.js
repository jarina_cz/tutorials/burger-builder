import React, {Fragment} from 'react';

import Button from '../../UI/Button/Button';

export default (props) => {
  const ingredientSummary = Object.keys(props.ingredients)
    .map(igKey => {
      return (
        <li key={igKey}>
          <span style={{textTransform: 'capitalize'}}>{igKey}: {props.ingredients[igKey]}</span>
        </li>
      )
    });
  return (
    <Fragment>
      <h3>Your Order</h3>
      <p>A delicious burger with the following ingredients:</p>
      <ul>
        {ingredientSummary}
      </ul>
      <p><strong>Total Price: {props.price.toFixed(2)}</strong></p>
      <p>Continue to Checkout?</p>
      <Button buttonType="Danger" clicked={props.purchaseCancelled}>Close</Button>
      <Button buttonType="Success" clicked={props.purchaseContinued}>Continue</Button>
    </Fragment>
  )
}
