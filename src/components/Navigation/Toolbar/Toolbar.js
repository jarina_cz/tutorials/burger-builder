import React from 'react';

import classes from './Toolbar.css';

import Logo from '../../Logo/Logo';

export default (props) => (
  <header className={classes.Toolbar}>
    <div>MENU</div>
    <Logo/>
    <nav>
      ...
    </nav>
  </header>
);
